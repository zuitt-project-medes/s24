//  Query Operators and Field Projection

/*
Mini-Activity

Open a shell in your new database called session 24

-Insert 5 products in a new products collection with the following details:

name- Iphone X
price- 30000
isActive- true

name- Samsung Galaxy S21
price- 51000
isActive- true

name- Razer Blackshark V2X
price- 2800
isActive- false

name- RAKK Gaming Mouse
price- 1800
isActive- true

name- Razer Mechanical Keyboard
price- 4000
isActive- true

*/

db.courses.insertMany([
    {
        "name" : "Iphone X",
        "price" : "30000",
        "isActive" : true
    },
    {
        "name" : "Samsung Galaxy S21",
        "price" : "51000",
        "isActive" : true
    },
    {
        "name" : "Razer Blackshark V2X",
        "price" : "2800",
        "isActive" : false
    }
    {
        "name" : "RAKK Gaming Mouse",
        "price" : "1800",
        "isActive" : true
    }
    {
        "name" : "Razer Mechanical Keyboard",
        "price" : "4000",
        "isActive" : true
    }
]);








